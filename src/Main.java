import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = myObj.nextLine();

        System.out.println("Last Name: ");
        String lastName = myObj.nextLine();


        System.out.println("First Subject Grade: ");

        double grade1 = myObj.nextDouble();
        System.out.println("Second Subject Grade: ");
        double grade2 = myObj.nextDouble();
        System.out.println("Third Subject Grade: ");
        double grade3 = myObj.nextDouble();
        System.out.println("Fourth Subject Grade: ");
        double grade4 = myObj.nextDouble();
        double average = (grade1+grade2+grade3+grade4)/4;
        System.out.println("Good day "+ firstName + lastName +".");
        System.out.println("Your grade average is:" + average);

    }
}